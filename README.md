### One pipeline configuration for all the microservices

Each service has a Dockerfile which starts the microservice container.
Each service can be deployed and run independently

With the configuration that we have, every time any one of the services gets changed - all 3 jobs will run in parallel.

Instead, we want to build only the service that has been changed.
We just need to add a condition to the job that says when to run it.
We can do that using the keyword only:. 
                only:
                    changes:
                    - "frontend/**/*"
Inside of that we can say, only when changes happen in the frontend folder, in any directory, any file inside the frontend folder, only then execute this job.
So if anything gets changed in a shopping-cart folder this will be ignored. 
So with only: we can say when the job should be executed.

### extends: .build

We are repeating a lot of code and configuration.  There is a lot of repetition. 
Let’s optimize and re-use this logic using extends: .

Let’s create a generic and hidden job called .build: .

                deploy_frontend:
                extends: .deploy
                variables:
                    MICRO_SERVICE: frontend
                    SERVICE_VERSION: "1.3"
                    APP_PORT: 3000
                only:
                    changes:
                    - "frontend/**/*"

We can pass parameters to our base job  - that we are  extending from - by using variables.
We can not use $MICRO_SERVICE variable in only: changes: attribute as a substitute for whatever service folder that is.
In GitLab variables do net get expended in certain places. and only: changes: attribute is one of those places.
So we will take that out from a generic job and we will have to put it in a specific build job.
And of course pipeline doesn’t get triggered because all of these changes are in the root, because gitlab-ci.yml is in the root, so we need to make changes in the service folder.


